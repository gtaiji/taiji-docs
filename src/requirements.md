# Requirements

 - user must see GIF of 24 forms
 - user must read instructions for every form
 - user must see contacts of heorgt, email
 
 - user must choose sit/stand/lie
 - user must choose axes/spirals/circles
 - user must choose hands/ban/mace
 - user must see pages with description of form - GIF, photos, video, text instructions
 
 - user must see history of watches
 - user can see favourite exercies
 
 - user must see a chart of practice history with accrued hours of practice
 
 - user must NOT register

 - user can see interactive counter with a pleasant sound for every repetitin of the form

 - user can see a sequence of exercises with recommendation for exercise
 - user can pass a test to get a test

 - user MUST NOT communicate with others

 - developer must add new axes to table

happy path
 - open app
 - choose exercise from the table 
 - view exercise
 - repeat exercise for 15 min with interactive counter
 - see history progress 
 - choose different exercise
 - repeat exercise for 15 min
 - see history progress
 - close app

 - open app
 - view contact "for personal help"
 - contact heorg
 - suscribe in tg

 - open app
 - view exercise
   - make notes
   - see history of notes
   - sync with calendar
   - see calendar diary
 - exit
 - view exercise
 - exit
 - view exercise
 - close app

# screens
 - home
   - choose exercise from table
   - choose exercise from favorite list 
   - view history
   - read journal
   - contact heorg
 - exercise page
 - history chart
 - journal
 - contact
